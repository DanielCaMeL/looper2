import java.util.Scanner;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

public class MainClass
{
    public static void main(String[] args)
    {
        int input;
        Choice transfer = new Choice();

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Which cave are you entering?");
        input = keyboard.nextInt();

        transfer.setChoice(input);

        System.out.println(transfer.getChoice());

    }
}