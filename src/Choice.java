public class Choice
{
    private int choice;

    public void setChoice(int cho)
    {
        choice = cho;
    }

    public String getChoice()
    {
        if(choice ==1)
        {
            return ("You entered cave 1, and the dragon now owns you!");
        }
        else if (choice == 2)
        {
            return ("You entered cave 2 and you got the treasure!");
        }
        else
            return ("Invalid choice");
    }

}